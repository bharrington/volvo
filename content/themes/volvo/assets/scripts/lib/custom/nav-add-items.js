$(document).ready(function() {
  // make a list of out groups
  (function () {
    var $list = $( 'section' );
    var lists = {};
    var $newLists = $();

    $list.children().each( function () {
      var date = $( this ).data( 'date' );
      if ( !lists[ date ] ) lists[ date ] = [];
      lists[ date ].push( this );
    });

    $.each( lists, function ( date, items ) {
      var $newList = $( "<section />").append( items ).addClass( date ).attr('id', date);
      $newLists = $newLists.add( $newList );
    });

    $list.replaceWith( $newLists );
  }());

  // clone and move to nav
  $("section").clone().appendTo("nav").empty();

  // use post date to populate text, id, link, etc
  var attrs = { };
  $.each($("nav  section")[0].attributes, function(idx, attr) {
    attrs[attr.nodeName] = attr.nodeValue;
  });

  $("nav section").replaceWith(function () {
    var postDate = $( this ).attr( 'class' );
    var postDateFormatted = $( this ).attr( 'class' ).replace(/\-/g, '/'); // replace - for /
    return $("<a />", attrs).append($(this).contents()).text(postDateFormatted).removeClass().attr('href', '#'+postDate);
  });

  // format text
  $('nav a').formatDateTime('MM yy');
});
