$(document).ready(function() {
  // add current to first
  $('nav a:first-of-type').addClass('current');

  // on click change current marker
  $( "nav a" ).click(function() {
    $("nav a").removeClass('current');
    $(this).addClass('current');

    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
  });

  var aChildren = $("nav").children(); // find the a children of the list items
    var aArray = []; // create the empty aArray
    for (var i=0; i < aChildren.length; i++) {
        var aChild = aChildren[i];
        var ahref = $(aChild).attr('href');
        aArray.push(ahref);
    } // this for loop fills the aArray with attribute href values

  $(window).scroll(function(){
    var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
    var windowHeight = $(window).height(); // get the height of the window
    var docHeight = $(document).height();

    for (var i=0; i < aArray.length; i++) {
      var theID = aArray[i];
      var divPos = $(theID).offset().top; // get the offset of the div from the top of page
      var divHeight = $(theID).height(); // get the height of the div in question
      if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
        $("a[href='" + theID + "']").addClass("current");
      } else {
        $("a[href='" + theID + "']").removeClass("current");
      }
    }

    if(windowPos + windowHeight == docHeight) {
      if (!$("nav a:last-child").hasClass("current")) {
        var navActiveCurrent = $(".current").attr("href");
        $("a[href='" + navActiveCurrent + "']").removeClass("current");
        $("nav a:last-child").addClass("current");
      }
    }
  });
});
