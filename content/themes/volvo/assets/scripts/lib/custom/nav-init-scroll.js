$(document).ready(function() {
  var navScroller = function() {
    $('nav').jScrollPane({contentWidth: '200px'});
    if ($(".jspScrollable").is(':visible')){
        $('body').addClass('nav-can-scroll');
    } else {
        $('body').removeClass('nav-can-scroll');
    }
  }

  // init
  navScroller();

  // onresize
  var resize;
  window.onresize = function(){
    clearTimeout(resize);
    resize = setTimeout(navScroller, 100);
  };
});
