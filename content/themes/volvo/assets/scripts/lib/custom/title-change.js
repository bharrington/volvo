var title = document.title,
newTitle = "Volvo misses you!";
document.addEventListener("visibilitychange", function() {
  document.title = ((document.hidden) ? newTitle : title);
});
