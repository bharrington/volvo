<?php
function volvo_custom_image() {
  add_image_size( 'full', 850, 9999 );
}

add_action( 'after_setup_theme', 'volvo_custom_image' );
