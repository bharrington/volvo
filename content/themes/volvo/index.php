<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <title><?php bloginfo('name'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <meta name="description" content="<?php bloginfo('description') ?>" />
  <?php wp_head(); ?>
</head>
<body>
  <h1><?php bloginfo('name'); ?></h1>
  <section>
    <?php $args = array(
      'posts_per_page'   => -1,
    );
    $posts_array = get_posts( $args ); ?>

    <?php if ( have_posts() ) while ( have_posts() ) : the_post();
      $img         = get_field('image');
      $img_resized = wp_get_attachment_image_src($img, 'full'); ?>

      <article data-date="<?php echo get_the_date('Y-m'); ?>">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/image-preload.png" data-src="<?php echo $img_resized[0]; ?>" />

        <?php if( get_field('info') ): ?>
          <p><?php the_field('info'); ?></p>
        <?php endif; ?>
      </article>
    <?php endwhile; wp_reset_query(); ?>
  </section>
  <nav></nav>
  <?php wp_footer(); ?>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-48113179-2', 'auto');
    ga('send', 'pageview');
  </script>
</body>
</html>
